$(document).ready(function(){
  var owl = $('.owl-carousel')
  owl.owlCarousel({
    items: 3,
    nav: true,
    navText : ['<span class="slider-controls" id="prev_img"></span>','<span class="slider-controls" id="next_img"></span>'],
    margin:30,
    pagination: false,
    loop: true,
    autoHeight:true,
    itemsDesktop : [1000,3], //5 items between 1000px and 901px
    itemsDesktopSmall : [900,2], // betweem 900px and 601px
    itemsTablet: [600,2], //2 items between 600 and 0
    temsMobile : false // itemsMobile disabled - inherit from itemsTablet option
  });
});
